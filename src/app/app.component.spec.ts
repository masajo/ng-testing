import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ListaAlumnosComponent } from './components/lista-alumnos/lista-alumnos.component';

describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        ListaAlumnosComponent
      ],
    }).compileComponents();

    // Antes de cada test, replanteamos el componente y su instancia "app"
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  // * R I T E --> Readable Isolated/Integrated Through Explicit
  // Las pruebas deben tener: contexto - acciones - verificaciones

  // * SUT --> System Under Tests (la unidad que se está probando)

  // Las pruebas tienen que estar enfocadas únicamente sobre lo que se está probando
  // En caso de que lo que se está probando tenga dependencias --> Hay que simularlas (mock, stubs, faked data, etc.)
  // El objetivo primordial es verificar que lo desarrollado es correcto, independientemente de las
  // dependencias y lo bien o mal que estén estas.

  // * TDD --> Metodología de Test Driven Development
  // Fase 1: Definición de pruebas que fallan (RED)
  // Fase 2: Implementación de código que hacen pasar las pruebas (GREEN)
  // Fase 3: Refactorización del código y de las pruebas (REFACTOR)
  // Es ciclo que no termina. Fase 1 --> Fase 2 --> Fase 3 --> Fase 1 --> etc.

  it('Debería crear el componente App', () => {
    expect(app).toBeTruthy(); // toBeTrue()
  });

  it(`Debería disponer una variable title con valor 'ng-testing'`, () => {
    expect(app.title).toEqual('ng-testing');
  });

  // Shallow Testing - Validaciones del HTML
  it('Debería renderizar el título en el <SPAN></SPAN>', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('ng-testing app is running!');
  });
});
